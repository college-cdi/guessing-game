const totalAttempts = 5;
let remainingAttempts = document.getElementById("remainingAttempts").innerHTML;
let solution = [];
let answer = { 0 : "Blue", 1 : "Blue", 2 : "Blue" };
let isGameWon = false;

/**
 * @description Function pour initialiser le jeu.
 */
function initGame() {
    console.log("Initialising game...")
    isGameWon = false;
    solution = [];
    let color = "";
    remainingAttempts = document.getElementById("remainingAttempts").innerHTML = totalAttempts;
    for ( let i = 0; i < 3; i++ ) {
        color = generateColor();
        solution.push({ "color" : color });
    }
}

/**
 * @description Génère la couleur pour chaque cercle.
 * @returns String
 */
function generateColor() {
    let colorNumber = Math.floor(Math.random() * ( 3 ) + 1);
    switch (colorNumber) {
        case 1: 
            return "Yellow";
        case 2:
            return "Red";
        case 3: 
            return "Blue";
        default: 
            return;
    }
}

/**
 * @description Vérifie la solution.
 */
function verify() {
    remainingAttempts = document.getElementById("remainingAttempts").innerHTML -= 1;
    let correctAnswer = 0;
    for (let i = 0; i < 3; i++) {
        if (answer[i] == solution[i].color) {
            correctAnswer += 1;
        }
    }
    document.getElementById("Score-count").innerHTML = correctAnswer;

    // correct Answer display 
    if (correctAnswer === 3) {
        document.getElementById("verify").disabled = true;
        alert("You won!");
        isGameWon = true;
        return;
    }
    // Attempts remaining display
    if (remainingAttempts <= 0) {
        document.getElementById("verify").disabled = true;
        alert("You lost. Try again?");
        document.getElementById("show-Solution").style.display = "block";
        return;
    }
}

/**
 * @description Change la couleur du cercle du joueur cliqué
 */
function guessColorChange(id) {
    let clickedElement = document.getElementsByClassName("circle player-guess")[id];

    switch(clickedElement.style.backgroundColor) {
        case "blue":
            answer[id] = "Red";
            clickedElement.style.backgroundColor = "red";
            break;
        case "red":
            answer[id] = "Yellow";
            clickedElement.style.backgroundColor = "yellow";
            break;
        case "yellow":
            answer[id] = "Blue"
            clickedElement.style.backgroundColor = "blue"
            break;
        default: 
            answer[id] = "Red";
            clickedElement.style.backgroundColor = "red";
            break;
    }
}

/**
 * @description Réinitialise la partie.
 */
function newGame() {
    document.getElementById("verify").disabled = false;
    document.getElementById("show-Solution").style.display = "none";
    document.getElementById("Score-count").innerHTML = "-";
    ShouldDisplaySolution(false);
    initGame();
}

/**
 * @description Montre ou cache la solution.
 */
function toggleShowSolution() {
    // S'assure que la visibilité de la solution peut être toggle que lorsque 
    if (remainingAttempts > 0 && isGameWon == false) {
        return;
    }
    if ( !document.getElementById("solution-circles").classList.contains("showing")) {
        document.getElementById("solution-circles").classList.add("showing");
        document.getElementById("button-show-solution").textContent = "Hide solution";
        ShouldDisplaySolution(true);
    } else {
        document.getElementById("solution-circles").classList.remove("showing");
        document.getElementById("button-show-solution").textContent = "Show solution";
        ShouldDisplaySolution(false);
    }
}

/**
 * @description Montre ou cache la couleur de chaque cercles de la solution.
 */
function ShouldDisplaySolution(shouldShow) {
    for (let i = 0; i < 3; i++) {
        if (shouldShow) {
            document.getElementsByClassName("circle solution")[i].style.backgroundColor = solution[i].color;
        } else {
            document.getElementsByClassName("circle solution")[i].style.backgroundColor = "black";
        }
    }
}

initGame();
